Summary:	Libtasn1 is a ASN.1 parsing library
Name:		libtasn1
Version:	4.16.0
Release:	1

# The libtasn1 library is LGPLv2+, utilities are GPLv3+
License:	GPLv3+ and LGPLv2+
URL:		http://www.gnu.org/software/libtasn1/
Source0:	http://ftp.gnu.org/gnu/libtasn1/%name-%version.tar.gz
Source1:	http://ftp.gnu.org/gnu/libtasn1/%name-%version.tar.gz.sig

BuildRequires:	gcc, autoconf, automake, libtool, gnupg2, bison, pkgconfig, help2man
%ifarch %{valgrind_arches}
BuildRequires:  valgrind-devel
%endif
Provides: bundled(gnulib) = 20130324
Provides: %{name}-tools = %{version}-%{release}
Obsoletes: %{name}-tools < %{version}-%{release}

%description
Libtasn1 is the ASN.1 library used by GnuTLS, p11-kit and some other packages.
The goal of this implementation is to be highly portable, and only require an
ANSI C99 platform.This library provides Abstract Syntax Notation One (ASN.1,
as specified by the X.680 ITU-T recommendation) parsing and structures management,
and Distinguished Encoding Rules (DER, as per X.690) encoding and decoding functions.

%package devel
Summary:	Development files for %{name}
Requires:	%{name}%{?_isa} = %{version}-%{release}
Requires:	pkgconfig
Requires:	info

%description devel
This package contains header files and so files for development.

%package_help

%prep
%autosetup -p1

%build
autoreconf -vfi
%configure --disable-static --disable-silent-rules

make %{?_smp_mflags}

%install
%make_install
%delete_la
rm -f $RPM_BUILD_ROOT%{_infodir}/dir

%check
make check

%post   -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%post help
test -f %_infodir/%name.info.gz && \
	/sbin/install-info --info-dir=%_infodir %_infodir/%name.info || :

%preun help
test "$1" = 0 -a -f %_infodir/%name.info.gz && \
	/sbin/install-info --info-dir=%_infodir --delete %_infodir/%name.info || :

%files
%license LICENSE doc/COPYING*
%doc AUTHORS NEWS README.md THANKS
%{_bindir}/asn1*
%{_libdir}/*.so.6*

%files devel
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_includedir}/*

%files help
%doc doc/TODO doc/*.pdf
%{_mandir}/man1/asn1*
%{_mandir}/man3/*asn1*
%{_infodir}/*.info.*

%changelog
* Sat Jul 25 2020 linwei <linwei54@huawei.com> - 4.16.0-1
- update libtasn1 to 4.16.0

* Sat Jun 6 2020 lirui <lirui130@huawei.com> - 4.13-9
- fix fuzz issues

* Fri Mar 29 2020 whoisxxx <zhangxuzhou4@huawei.com> - 4.13-8
- Valgrind support specific arches 

* Fri Mar 20 2020 wangye <wangye54@huawei.com> - 4.13-7
- Fix CVE-2018-1000654

* Sun Jan 12 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.13-6
- simplify functions

* Fri Sep 06 2019 openEuler Buildteam <buildteam@openeuler.org> - 4.13-5
- Package init
